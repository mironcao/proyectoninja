// This is `services/fireinit.js`

import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/database'

const config = {
  apiKey: 'AIzaSyDadm29f7NQcEGdoa9nm_M81DCdD2nyJqM',
  authDomain: 'proyectoninja-8249e.firebaseapp.com',
  databaseURL: 'https://proyectoninja-8249e.firebaseio.com',
  projectId: 'proyectoninja-8249e',
  storageBucket: 'proyectoninja-8249e.appspot.com',
  messagingSenderId: '265952239168'
}

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

export const GoogleProvider = new firebase.auth.GoogleAuthProvider()
export const auth = firebase.auth()
export const DB = firebase.database()
export const StoreDB = firebase.firestore()
export default firebase
